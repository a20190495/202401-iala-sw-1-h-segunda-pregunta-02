#include <iostream>

using namespace std;


int** createMatrix(int row, int colom);
void printMatrix(int row, int colom, int** matrix);

int main() {


	int row, colom;
	cout << "Ingrese la cantida de filas para la matriz:";
	cin >> row;
	cout << "Ingrese la cantida de columnas para la matriz:";
	cin >> colom;

	int** matrix = createMatrix(row, colom);

	printMatrix(row, colom, matrix);

	return EXIT_SUCCESS;
}


int** createMatrix(int row, int colom) {

	int** matrix = new int* [row];

	for (int i = 0; i < row; ++i) {
		matrix[i] = new int[colom];
	}

	return matrix;
}

void printMatrix(int row, int colom, int** matrix) {

	cout << "Matriz 10x15 llenada por 0's" << endl;

	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < colom; ++j) {
			cout << matrix[i][j] << ' ';
		}
		cout << endl;
	}
}