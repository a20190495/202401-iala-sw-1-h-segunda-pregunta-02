#include <iostream>
#include <random>

using namespace std;


int* createArray(int cant_Random);

int main() {

	random_device rd;
	int cant_Random = randint(100, 501, rd);
	int* array = createArray(cant_Random);

	return EXIT_SUCCESS;
}

int randint(int a, int b, random_device &rd) {
	mt19937_64 gen(rd());
	return gen() % (b - a) + a;
}

int* createArray(int cant_Random) {
	
	random_device rd;
	int* arrayTemp = new int [cant_Random];
	for (int i = 0; i < cant_Random; ++i) {
		int elementRandom = randint(1, 10000, rd);
		arrayTemp[i] = elementRandom;
	}
	return arrayTemp;
}
